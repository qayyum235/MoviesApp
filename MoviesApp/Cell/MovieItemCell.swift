//
//  MovieItemCell.swift
//  MoviesApp
//
//  Created by sid on 11/21/23.
//

import UIKit
import Nuke

class MovieItemCell: UITableViewCell {

    @IBOutlet weak var buttonDownload: UIButton!
    @IBOutlet weak var labelEpisodeName: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(indexPath: IndexPath){
        
        Nuke.loadImage(with: getUrlFromString(stringUrl: SAMPLE_IMAGE_URL), options: IMAGE_OPTIONS,into: self.thumbnailImageView)

        
        self.labelEpisodeName.text = "E\(indexPath.row + 1) - \(APIService.instance.tvShowDetailData?.episodes?[indexPath.row].name ?? "")"
    }
    
}
