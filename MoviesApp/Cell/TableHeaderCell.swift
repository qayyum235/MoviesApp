//
//  TableHeaderCell.swift
//  MoviesApp
//
//  Created by sid on 11/22/23.
//

import UIKit

class TableHeaderCell: UICollectionViewCell {
    
    @IBOutlet weak var labelSeasonNumber: UILabel!
    
    @IBOutlet weak var viewSeperator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(indexPath: IndexPath , selectedTabIndex: Int) {
        
        self.labelSeasonNumber.text = "SEASON \(indexPath.row + 1)"
        
        if indexPath.row == selectedTabIndex {
            self.labelSeasonNumber.textColor = .white
        }
        else {
            self.labelSeasonNumber.textColor = .lightGray
        }
    }
    
   
}
