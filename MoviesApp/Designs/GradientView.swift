//
//  GradientView.swift
//  MoviesApp
//
//  Created by sid on 11/24/23.
//

import Foundation
import UIKit

@IBDesignable
class GradientView: UIView {
    @IBInspectable var firstColor: UIColor = UIColor.clear {
        didSet {
        updateView()
    }
}

    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
    override class var layerClass: AnyClass {
       get {
          return CAGradientLayer.self
       }
    }
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor].map{$0.cgColor}
        layer.startPoint = CGPoint(x: 0, y: 0.0)
        layer.endPoint = CGPoint (x: 0, y: 0.5)
    }
    

}
