//
//  APIService.swift
//  MoviesApp
//
//  Created by sid on 11/22/23.
//

import Foundation

class APIService {
    
    static let instance = APIService()
    
    var tvShowData: TvShowModel?
    var tvShowDetailData: TvShowDetailModel?
    
    
    
    func getTVShow(seriesId: Int, completion: @escaping completionHandler){
        let url = "\(BASE_URL)\(seriesId)?\(API_KEY)"
        
        RequestService.instance.getRequest(url: url, header: [:]) { Success,response in
            
            if Success {
                guard let responseData = response else {
                    ErrorService.instance.message = JSON_PARSING_ERROR
                    completion(false)
                    return
                }
                do {
                    self.tvShowData = try JSONDecoder().decode(TvShowModel.self, from: responseData )
                    completion(true)
                }
                catch {
                    ErrorService.instance.message = JSON_PARSING_ERROR
                    completion(false)
    
                }
            }
            else {
                completion(false)
            }
        }
        
    }
    
    func getTVShowDetail(seriesId: Int, seasonNumber: Int, completion: @escaping completionHandler){
        let url = "\(BASE_URL)\(seriesId)/season/\(seasonNumber)?\(API_KEY)"
        
        RequestService.instance.getRequest(url: url, header: [:]) { Success,response in
            completion(Success)
            
            if Success {
                guard let responseData = response else {
                    ErrorService.instance.message = JSON_PARSING_ERROR
                    completion(false)
                    return
                }
                do {
                    self.tvShowDetailData = try JSONDecoder().decode(TvShowDetailModel.self, from: responseData )
                    completion(true)
                }
                catch {
                    ErrorService.instance.message = JSON_PARSING_ERROR
                    completion(false)
    
                }
            }
            else {
                completion(false)
            }
        }
        
    }
}
