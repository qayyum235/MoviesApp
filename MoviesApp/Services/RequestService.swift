//
//  RequestService.swift
//  MoviesApp
//
//  Created by sid on 11/22/23.
//

import Foundation
import Alamofire
import SwiftyJSON

class RequestService {
    static let instance = RequestService()

    func getRequest( url: String ,header : HTTPHeaders, completion: @escaping completionHandlerWithResponse) {
        if Reachability.isConnectedToNetwork() {
            AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success:
                        //let resJSON = JSON(response.value)
                        guard let data = response.data else {
                            ErrorService.instance.message = NO_RESPONSE_ERROR
                            completion(false,nil)
                            return
                        }
                                                
                        completion(true,data)
                        break
                    case .failure(let error):
                        //print(error)
                        switch (error._code){
                            case NSURLErrorTimedOut:
                                //Manager your time out error
                                Reachability.isInternetWorking { (success) in
                                    if success {
                                        
                                        ErrorService.instance.message = NOT_ABLE_TO_REACH_SERVER_ERROR
                                        completion(false,nil)
                                    }
                                    else {
                                        ErrorService.instance.message = NOT_STABLE_INTERNET_ERROR
                                        completion(false,nil)
                                    }
                                }
                                
                                break
                            case NSURLErrorNotConnectedToInternet:
                                //Manager your not connected to internet error
                                ErrorService.instance.message = NO_INTERNET_ERROR
                                completion(false,nil)
                                break
                            default:
                                //manager your default case
                                print("errorcode---, \(response.error!)")
                                print("errorcode---, \(response.error!._code)")
                                print("errorcode---, \(response.error!.localizedDescription)")
                                ErrorService.instance.message = response.error!.localizedDescription
                            completion(false,nil)
                            }
                        break
                    }
                })
        }
        else {
            ErrorService.instance.message = NO_INTERNET_ERROR

            completion(false,nil)
        }
        
    }
}
