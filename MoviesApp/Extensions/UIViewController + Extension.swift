//
//  UIViewController + Extension.swift
//  MoviesApp
//
//  Created by sid on 11/24/23.
//

import Foundation
import UIKit

extension UIViewController {
    func alertMessage(title:String,message:String) {
        let alertcontroler=UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertcontroler.addAction(UIAlertAction(title:"OK", style: .default, handler:nil))
        present(alertcontroler, animated: true, completion: nil)
    }
    
    
}
