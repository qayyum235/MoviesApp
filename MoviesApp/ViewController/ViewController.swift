//
//  ViewController.swift
//  MoviesApp
//
//  Created by sid on 11/21/23.
//

import UIKit
import Nuke

class ViewController: UIViewController {
    
    
    var selectedTabIndex = 0
    let seriesId =  62852
    
    @IBOutlet weak var imageViewBanner: UIImageView!
    @IBOutlet weak var labelSeasonName: UILabel!
    @IBOutlet weak var labelSeasonYear: UILabel!
    @IBOutlet weak var labelTotalSeasons: UILabel!
    @IBOutlet weak var labelSeasonOverview: UILabel!
    
    @IBOutlet weak var labelGenres: UILabel!
    @IBAction func btnPlayClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "goToVideoDetailVC", sender: self)

    }
    
    @IBAction func btnTrailerClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "goToVideoDetailVC", sender: self)

    }
    
    @IBAction func btnWatchListClicked(_ sender: Any) {
    }
    
    @IBAction func btnLikeClicked(_ sender: Any) {
        
    }
    
    @IBAction func btnDisLikeClicked(_ sender: Any) {
    }
    
    
    

    @IBOutlet weak var tableView: UITableView!
    
    var headerView: UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setUpUI()
    }

    
    func setUpUI(){
        // register table view cell
        self.tableView.register(UINib(nibName: "MovieItemCell", bundle: nil), forCellReuseIdentifier: "moviecellidentifier")
        //configure table header
        self.configureHeaderView()
        // table view delegates
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.labelSeasonOverview.isUserInteractionEnabled = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // download show data
        self.getTVShowData()
    }
    
    
    
    func getTVShowData(){
        LoadingView.shared.showOverlay(view: self.view)
        APIService.instance.getTVShow(seriesId: self.seriesId) { Success in
            if Success {
                // download show detail data
                self.gettvShowDetailData(seasonNumber: 1)
                // set up data on UI
                self.setUpData()
            }
            else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    LoadingView.shared.hideOverlayView()
                    self.alertMessage(title: "Error", message: ErrorService.instance.message)
                }
            }
        }
    }
    
    func gettvShowDetailData(seasonNumber: Int){
        
        APIService.instance.getTVShowDetail(seriesId: self.seriesId, seasonNumber: seasonNumber) { Success in
            if Success {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    LoadingView.shared.hideOverlayView()
                    self.tableView.reloadData()
                }
                
            }
            else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    LoadingView.shared.hideOverlayView()
                    self.alertMessage(title: "Error", message: ErrorService.instance.message)
                }
            }
        }
    }
    
    
    func setUpData() {
        let data = APIService.instance.tvShowData
        
        Nuke.loadImage(with: getUrlFromString(stringUrl: SAMPLE_IMAGE_URL), options: IMAGE_OPTIONS,into: self.imageViewBanner)

        
        self.labelSeasonName.text = data?.original_name ?? ""
        
        let year = data?.first_air_date?.split(separator: "-") ?? []
        self.labelSeasonYear.text = "\(year[0])"
        self.labelTotalSeasons.text = "\(data?.number_of_seasons ?? 0) Seasons"
        self.labelGenres.text = data?.genres?[0].name ?? ""
        self.labelSeasonOverview.appendReadmore(after: data?.overview ?? "", trailingContent: .readmore)
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapLabel))
        self.labelSeasonOverview.addGestureRecognizer(tap)
        
        self.tableView.reloadData()
        self.headerView?.reloadData()
    }
    
    
    @IBAction func didTapLabel(_ sender: UITapGestureRecognizer) {
        guard let text = self.labelSeasonOverview.text else { return }

            let readmore = (text as NSString).range(of: TrailingContent.readmore.text)
            let readless = (text as NSString).range(of: TrailingContent.readless.text)
            if sender.didTap(label: self.labelSeasonOverview, inRange: readmore) {
                self.labelSeasonOverview.appendReadLess(after:  APIService.instance.tvShowData?.overview ?? "", trailingContent: .readless)
            } else if  sender.didTap(label: self.labelSeasonOverview, inRange: readless) {
                self.labelSeasonOverview.appendReadmore(after: APIService.instance.tvShowData?.overview ?? "", trailingContent: .readmore)
            } else { return }
            
        }

}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let head = tableView.headerView(forSection: section)
        head?.layer.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: tableView.frame.width, height: 30.0))
        return head
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APIService.instance.tvShowDetailData?.episodes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "moviecellidentifier", for: indexPath) as? MovieItemCell else { fatalError("Unexpected Table View Cell") }
        
        cell.configureCell(indexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "goToVideoDetailVC", sender: self)
        
    }
    
    
    
}


extension ViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func configureHeaderView() {

            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

            self.headerView = UICollectionView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 30), collectionViewLayout: layout)
            self.headerView?.backgroundColor = .black
            self.headerView?.isPagingEnabled = true
            self.headerView?.isUserInteractionEnabled = true
            self.headerView?.dataSource = self
            self.headerView?.delegate = self
        
            self.headerView?.register(UINib(nibName: "TableHeaderCell", bundle: nil), forCellWithReuseIdentifier: "tableheaderidentifier")
    
            self.headerView?.showsHorizontalScrollIndicator = false
            self.tableView.tableHeaderView = headerView
           
    
        }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return APIService.instance.tvShowData?.number_of_seasons ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tableheaderidentifier", for: indexPath) as? TableHeaderCell else { fatalError("Unexpected Table View Cell") }
        
        cell.configureCell(indexPath: indexPath, selectedTabIndex: self.selectedTabIndex)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
        self.selectedTabIndex = indexPath.row
        self.headerView?.reloadData()
        LoadingView.shared.showOverlay(view: self.view)
        self.gettvShowDetailData(seasonNumber: indexPath.row + 1)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/3, height: 30)
    }
    
    
    
}
