//
//  VideoDetailVC.swift
//  MoviesApp
//
//  Created by sid on 11/23/23.
//

import UIKit
import AVFoundation
import AVKit

class VideoDetailVC: UIViewController {

    var player : AVPlayer!
    var avPlayerController : AVPlayerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setUpVideoContent()
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBOutlet weak var videoView: UIView!
    
    func setUpVideoContent(){
        let videoURL = getUrlFromString(stringUrl: VIDEO_CONTENT_URL)
        
        self.player = AVPlayer(url: videoURL)
        self.avPlayerController = AVPlayerViewController()
        self.avPlayerController.player = self.player
        self.avPlayerController.view.frame = CGRect(x: 0, y: 0, width: self.videoView.layer.frame.width, height: 300)
        
        self.avPlayerController.showsPlaybackControls = true
        
        self.videoView.addSubview(avPlayerController.view)
        
        self.avPlayerController.player?.play()
        
    }

}
