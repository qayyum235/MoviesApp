//
//  Constants.swift
//  MoviesApp
//
//  Created by sid on 11/22/23.
//

import Foundation
import SwiftyJSON
import Alamofire
import Nuke

typealias completionHandler = (_ Success:Bool) ->()
typealias completionHandlerWithResponse = (_ Success:Bool, _ data: Data?) ->()

let IMAGE_OPTIONS = ImageLoadingOptions(
    placeholder: UIImage(named: "placeholder"),
    failureImage: UIImage(named: "placeholder")
)

func getUrlFromString(stringUrl: String) -> URL {
    return URL(string: stringUrl)!
}
// SAMPLE IMAGE
let SAMPLE_IMAGE_URL = "https://images.pexels.com/photos/303383/pexels-photo-303383.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"

// VIDEO CONTENT
let VIDEO_CONTENT_URL = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"

// API_KEY
let API_KEY = "api_key=3d0cda4466f269e793e9283f6ce0b75e"


// API ENDPOINTS

let BASE_URL = "https://api.themoviedb.org/3/tv/"






// ERRORS
let NO_INTERNET_ERROR = "No Internet Connection. Please try again"
let NOT_ABLE_TO_REACH_SERVER_ERROR = "Not able to reach server. Please try again!"
let NOT_STABLE_INTERNET_ERROR = "Internet connection is not stable"
let JSON_PARSING_ERROR = "Json parsing Error."
let NO_RESPONSE_ERROR = "Empty response."



