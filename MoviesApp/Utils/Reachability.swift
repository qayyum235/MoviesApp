//
//  Reachability.swift
//  MoviesApp
//
//  Created by sid on 11/22/23.
//

import Foundation
import SystemConfiguration

open class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    
    class func isInternetWorking(completionHandler:@escaping (_ internet:Bool) -> Void)
        {
            let url = getUrlFromString(stringUrl: "http://www.google.com/")
            var req = URLRequest.init(url: url)
            req.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
            req.timeoutInterval = 4.0

            let task = URLSession.shared.dataTask(with: req) { (data, response, error) in


                if error != nil  {
                    completionHandler(false)
                } else {
                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200 {
                            completionHandler(true)
                        } else {

                            completionHandler(false)
                        }
                    } else {
                        completionHandler(false)
                    }
                }
            }
            task.resume()
          }
}
